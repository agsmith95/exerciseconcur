// g++ -std=c++17 spinlock.cpp -o spinlock

#include <atomic>
#include <thread>
#include <iostream>

class SpinLock {
private:
    std::atomic_flag flag = ATOMIC_FLAG_INIT;
public:
    void lock() {
        flag.test_and_set();
    }
    void unlock() {
        flag.clear();
    }
};

SpinLock spin;
int resource[]={2,4,6};

void workOnResource(int *res, size_t n, int v) {
    spin.lock();
    resource[n] = v; // work on resource
    spin.unlock();
}

int main() {
    std::thread t1(workOnResource, resource, 1,5);
    std::thread t2(workOnResource, resource, 1,3);

    t1.join();
    t2.join();

    std::cout   << '[' 
                << resource[0] << ", " 
                << resource[1] << ", " 
                << resource[2] 
                << "]\n";
}
