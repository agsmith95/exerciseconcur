// use gcc-trunk for now with -std=c++2a
// I "compiled" it in godbolt.org

#include <iostream>
#include <thread>
#include <chrono>

void doWork(std::stop_token st, char a) {
    while (!st.stop_requested()) {
        std::cout << a << '\n';
    }
}

int main() {
    std::jthread t1{doWork, 'A'},
                 t2{doWork, 'B'};
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}
